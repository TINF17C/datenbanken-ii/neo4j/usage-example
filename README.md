# A Neo4j usage example
This project aims to give a practical insight into the use of Neo4j.
This will be done in the form of a simple step-by-step tutorial.

First we want to get an instance up and running.
After that we are going to import a Spotify dataset into the database.
It contains the Spotify rankings of each day from January 2019 to February 2020 in the region Germany.
At the end we want to give you some example queries with which you can start!

## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:
- Cypher-Shell - [Download & Install](https://github.com/neo4j/cypher-shell/releases). OSX and Linux machines can typically install it with their package manager.

## Installation
There are multiple ways to get a Neo4j instance up and running:
- Docker
- [Native installation](https://neo4j.com/download/):
  - on Unix systems: package via package manager
  - on Windows: installer (*.exe)
- [Neo4j Aura](https://neo4j.com/aura/)
- [Compile it yourself](https://github.com/neo4j/neo4j)

We prefer to use docker:

`docker run -d -p 7474:7474 -p 7687:7687 --env NEO4J_AUTH=neo4j/password --name neo4j neo4j:latest`
This command maps the ports 7474 and 7687 to our local system, so that we can access the user interface at http://localhost:7474.
Also it sets a user and password to access the database.
If you want to persist the data, just have a look in the [Neo4j documentation](https://neo4j.com/developer/docker-run-neo4j/) and map some volumes to the host system.
If you choose another installation method, we refer to the [Neo4j documentation](https://neo4j.com/docs/operations-manual/current/installation/).

No matter what type of installation you have chosen, it is important to know where the `$NEO4J_HOME` directory is.
Typically it is the root directory of the installation.
Again we are refering to the [Neo4j documenation](https://neo4j.com/docs/operations-manual/current/configuration/file-locations/).

## Importing the data
We developed a graph structure to import the data.
This structure is shown in the following figure:
![Graph Structure](assets/graph-structure.png "Graph Structure")


This repository contains three important files:
```
.
+-- assets          
+-- artist.csv      --> Contains all artists in the dataset
+-- ranking.csv     --> Contains all ranking in the dataset
+-- README.md
+-- track.csv       --> Contains all tracks in the dataset
```
Please make sure to download these files or to clone this repository.
If you did it, you need to copy these to the `$NEO4J_HOME/import` directory.
If you use docker you have to execute these commands:
```bash
> docker cp artist.csv neo4j:/var/lib/neo4j/import
> docker cp ranking.csv neo4j:/var/lib/neo4j/import
> docker cp track.csv neo4j:/var/lib/neo4j/import
```

After you have done this we can now import the data into the database!
First of all you need to connect to the database with the Cypher-Shell.
Therefore execute `cypher-shell -u neo4j -p password` from the command line.

If you have successfully connected, we will clean up the database for safety's sake:
```bash
MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n,r;
```
Now we are going to import some artists into the database:
```bash
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///artist.csv" AS row
CREATE (:Artist {artist_id: row.id, name: row.artist});
```

If an error message similar to `Couldn't load the external resource at: file:/var/lib/neo4j/import/artist.csv` occurs, please make sure that your Neo4j instance has read permissions to the file!

Next we are going to import the tracks and the ranking in relation with region (`LISTED_IN`):
```bash
# Importing tracks
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///track.csv" AS row
CREATE (:Track {track_id: row.id, name: row.track_name});

# Importing ranks with in relation with region (see structure above)
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///ranking.csv" AS row
CREATE (rank:Ranking {ranking_id: row.id, rank: row.rank, track_id: row.track_id, artist_id: row.artist_id, no_streams: row.no_streams, url: row.url, stream_date: row.stream_date})
MERGE(region:Region {region: row.region})
CREATE(rank)-[country:LISTED_IN]->(region);
```

Now we are creating an index for faster graph querying:
```bash
CREATE INDEX ON :Artist(artist_id);
CREATE INDEX ON :Ranking(artist_id);
CREATE INDEX ON :Ranking(track_id);
CREATE INDEX ON :Track(track_id);
```

Next we are going to import the relationship `CREATED_BY` between an artist and a rank:
```bash
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///artist.csv" AS row
MATCH(a:Artist {artist_id: row.id})
MATCH(r:Ranking {artist_id: row.id})
MERGE (r)-[:CREATED_BY]->(a);
```

The last missing relationship is the `PLACED_IN` relationship between a ranking and a track:
```bash
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///track.csv" AS row
MATCH(t:Track {track_id: row.id})
MATCH(r:Ranking {track_id: row.id})
MERGE (t)-[:PLACED_IN]->(r);
```

After this is done, you can exit the cypher-shell with `:exit`.
Now visit http://localhost:7474 and fire some queries!

# Example queries
As we finally imported the Spotify dataset, let's fire some queries!
An example query can look like the following:

```bash
MATCH (tracks:Track)-[:PLACED_IN]-(r:Ranking)-[:CREATED_BY]-(a:Artist {artist_id: '483'})
RETURN tracks, r, a;
```

This query gets all tracks of the artist with the id 483 and the corresponding rank. The result looks as follows: 

![Result](assets/result-example-query.png "Result")

Here are more example queries. Now start cyphering!

```bash
# Get all tracks ranked on place 1 of a specific artist
MATCH (t:Track)-[:PLACED_IN]->(r:Ranking)-[:CREATED_BY]->(a:Artist)
WHERE r.rank = '1' and a.artist_id = '483'
RETURN r, t
ORDER BY r.no_streams DESC;

# Get all tracks ranked on place 1 of a specific artist as a table
MATCH (t:Track)-[:PLACED_IN]->(r:Ranking)-[:CREATED_BY]->(a:Artist)
WHERE r.rank = '1' and a.artist_id = '483'
RETURN t.name, count(r) AS ChartTop1
ORDER BY ChartTop1 DESC
```
